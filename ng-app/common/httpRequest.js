app.factory('httpRequest', ['$http', function ($http) {

    var baseUrl = "http://localhost:3000"
    var request = function (options) {
        options.url = baseUrl + options.url
        return $http(options);
    };

    return {
        request: request
    };

}]);