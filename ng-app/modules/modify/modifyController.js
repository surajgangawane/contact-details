app.controller("modifyController", ["$scope", "$location", "$timeout", "httpRequest", "Contact", function ($scope, $location, $timeout, httpRequest, Contact) {

    $scope.contact = Contact.getContact();
    
    $scope.errorMsg = "";
    
    $scope.updateContact = function (contact) {
        
        httpRequest.request({
            method: 'PUT',
            url: '/updateContactDetails',
            headers: {
                'content-type': 'application/json'
            },
            dataType: 'json',
            data: {
                firstName: contact.firstName,
                lastName: contact.lastName,
                contactNumber: contact.contactNumber,
                emailId: contact.emailId,
                status: contact.status
            }
        }).then(function (res) {
            if (res.status == 200) {
                $location.path("/");
            }
        }, function (err) {
            if (err.data && err.data.message) {
                $scope.errorMsg = err.data.message;
            } else {
                $scope.errorMsg = err.statusText || "Something went wrong."
            }
            $timeout(function () {
                $scope.errorMsg = "";
            }, 3000);
        });
    };

    $scope.cancel = function () {
        $location.path("/");
    }

}]);