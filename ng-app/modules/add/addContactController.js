app.controller("addContactController", ["$scope", "$location", "httpRequest", "$timeout", function ($scope, $location, httpRequest, $timeout) {
    
    $scope.errorMsg = "";
    
    $scope.addContact = function (contact) {
        
        httpRequest.request({
            method: 'POST',
            url: '/addNewContact',
            headers: {
                'content-type': 'application/json'
            },
            params: {},
            dataType: 'json',
            data: contact
        }).then(function (res) {
            if (res.status == 200) {
                $location.path("/");
            }
        }, function (err) {
            if (err.data && err.data.message) {
                $scope.errorMsg = err.data.message;
            } else {
                $scope.errorMsg = err.statusText || "Something went wrong."
            }
            $timeout(function () {
                $scope.errorMsg = "";
            }, 3000);

        });
    };

    $scope.cancel = function () {
        $location.path("/");
    }

}]);