app.factory('Contact', [function () {
    var contact = {};

    var getContact = function () {
        return contact;
    };

    var setContact = function (data) {
        contact = data;
    };

    return {
        getContact: getContact,
        setContact: setContact
    };
}]);
