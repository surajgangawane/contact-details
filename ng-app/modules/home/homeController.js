app.controller("homeController", ['$scope', '$location', '$timeout', 'httpRequest', 'Contact', function ($scope, $location, $timeout, httpRequest, Contact) {
    
    $scope.contacts = {};

    $scope.errorMsg = "";

    $scope.successMsg = "";
    
    var init = function () {
        httpRequest.request({
            method: 'GET',
            url: '/getAllContactDetails',
            headers: {
                'content-type': 'application/json'
            },
            params: {},
            dataType: 'json',
        }).then(function (res) { // Success callback
            $scope.contacts = res.data || [];
        }, function (err) { // Error Callback
            if (err.data && err.data.message) {
                $scope.errorMsg = err.data.message;
            } else {
                $scope.errorMsg = err.statusText || "Something went wrong."
            }
            $timeout(function () {
                $scope.errorMsg = "";
            }, 3000);
        });
    }

    init();

    $scope.addContact = function () {
        $location.path("/add")
    };

    $scope.updateContact = function (data) {
        Contact.setContact(data);
        $location.path("/update")
    };

    $scope.deleteContact = function (contact) {
        httpRequest.request({
            method: 'GET',
            url: '/deleteContactDetail',
            headers: {
                'content-type': 'application/json'
            },
            params: {
                contactNumber: contact.contactNumber
            },
            dataType: 'json'
        }).then(function (res) { // Success callback
            if (res.status == 200) {
                $scope.successMsg = res.data && res.data.message || "Contact Deleted Successfully!!!";
                $timeout(function () {
                    $scope.successMsg = "";
                    init();
                }, 3000);
            }
        }, function (err) { // Error Callback
            if (err.data && err.data.message) {
                $scope.errorMsg = err.data.message;
            } else {
                $scope.errorMsg = err.statusText || "Something went wrong."
            }
            $timeout(function () {
                $scope.errorMsg = "";
            }, 3000);
        });
    }

}]);