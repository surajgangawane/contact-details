// Creating angular app.
var app = angular.module("crudAPP", ["ngRoute", "ui.bootstrap"]);

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: 'modules/home/home.html',
        controller: 'homeController'
    }).when('/add', {
        templateUrl: 'modules/add/addContactView.html',
        controller: 'addContactController'
    }).when('/update', {
        templateUrl: 'modules/modify/modifyView.html',
        controller: 'modifyController'
    }).otherwise({
        redirectTo: "/"
    });
}]);