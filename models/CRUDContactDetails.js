var Joi = require('joi');
var database = require('./database');

const contactSchema = Joi.object().keys({
	firstName : Joi.string().required(),
	lastName : Joi.string().required(),
	emailId : Joi.string().email().required(),
	contactNumber : Joi.number().integer(),
	status : Joi.string(),
})


function addNewContact(data, res) {
	if (data) {
		Joi.validate(data, contactSchema, function(err, result) {
			if (err) {
				res.status(500).send({
					'message' : 'Please enter required fields'
				})
			} else {
				database.addNewContactDetailsToDB(result, res);
			}
		})
	}
}

function searchContact(req, res) {
	const contactNumber = req.query.contactNumber;
	database.getContactNumberFromDB(contactNumber, function(err, result) {
		if (err) {
			res.status(500).send(err);
		} else {
			res.send(result);
		}
	});
}

function deleteContact(req, res) {
	const contactNumber = req.query.contactNumber;
	database.delContactNumberFromDB(contactNumber, res)
}

function updateContactDetail(data, res) {
	if (data) {
		Joi.validate(data, contactSchema, function(err, result) {
			if (err) {
				res.status(500).send({
					'message' : 'Invalid data input'
				})
			} else {
				database.updateContactDetailsToDB(result, res);
			}
		})
	}
}

module.exports.addNewContact = addNewContact;
module.exports.searchContact = searchContact;
module.exports.deleteContact = deleteContact;
module.exports.updateContactDetail = updateContactDetail;