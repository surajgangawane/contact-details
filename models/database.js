var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var contactDetailsSchema = new Schema ({
	firstName : {type : String},
	lastName : {type : String},
	emailId : {type : String},
	contactNumber : {type : Number},
	status : {type : String}
})

const connection = mongoose.createConnection('mongodb://localhost/my_database');

var ContactModel = connection.model ('contact_details', contactDetailsSchema);

function addNewContactDetailsToDB(data, res) {
	const addNewContactDetails = new ContactModel(data);
	getContactNumberFromDB (data.contactNumber, function (err, result) {
		if (result) {
			res.status(500).send({
				'message' : 'Data with this contact number already exists'
			});
		} else if (err && err.status === 500){
			res.status(500).send({
				'message' : 'Internal Server 500 error. Please try again'
			});
		} else {
			addNewContactDetails.save(function(err) {
				if (err) {
					res.status(500).send({
						'message' : 'Internal Server 500 error. Please try again'
					})
				} else {
					res.send({
						'message' : 'Saved data successfully',
						'status' : 200
					})
				}
			})		
		}
	})
}

function getContactNumberFromDB (contactNumber, cb) {
	var errObj = null;
	if (contactNumber) {
		ContactModel.findOne({contactNumber : contactNumber}, function(err, result){
			if (err) {
				errObj = {
					'message' : 'Internal Server 500 error. Please try again',
					'status' : 500
				}
				cb(errObj, null);
			} else if(!result) {
				errObj = {
					'message' : 'Not Found',
					'status' : 404
				}
				cb(errObj, null);
			} else {
				cb(null, result)
			}
		})
	}
}

function delContactNumberFromDB (contactNumber, res) {
	if (contactNumber) {
		ContactModel.findOneAndRemove({contactNumber : contactNumber}, function(err, result) {
			if (err) {
				res.status(500).send({
					'message' : 'Internal Server 500 error. Please try again'
				})
			} else if (!result) {
				res.status(500).send({
					'message' : 'Internal Server 500 error. Please try again'
				})
			} else {
				res.send({
					'message' : 'Deleted contact successfully'
				})
			}
		})
	}
}

function updateContactDetailsToDB (data, res) {
	ContactModel.findOneAndUpdate({contactNumber : data.contactNumber}, data, function (err, result) {
		if (err) {
			res.status(500).send({
				'message' : 'Internal Server 500 error. Please try again'
			})
		} else if(!result) {
			res.status(500).send({
				'message' : 'You cannot edit contact number'
			})
		} else {
			res.status(200).send({
				'message' : 'Data updated successfully'
			})
		}
	})
}

function getAllData (req, res) {
	ContactModel.find(function(err, result){
		if (err) {
			res.status(500).send({
				'message' : 'Internal Server 500 error. Please try again'
			})
		} else {
			res.send(result);
		}
	})
}

module.exports.addNewContactDetailsToDB = addNewContactDetailsToDB;
module.exports.getContactNumberFromDB = getContactNumberFromDB;
module.exports.delContactNumberFromDB = delContactNumberFromDB;
module.exports.updateContactDetailsToDB = updateContactDetailsToDB;
module.exports.getAllData = getAllData