var express = require('express');
var bodyParser = require('body-parser');
var contactDetails = require('./models/CRUDContactDetails')
var contactDatabase = require('./models/database')

var app = express();

app.use(bodyParser.json());

app.use(express.static(__dirname + '/ng-app'))

app.get('/getAllContactDetails',function(req, res) {
	contactDatabase.getAllData(req, res);
});

app.post('/addNewContact', function(req, res) {
	if (req.body) {
		contactDetails.addNewContact(req.body, res);
	} else {
		res.status(500).send({
			'message' : 'Please provide details'
		});
	}
});

app.put('/updateContactDetails', function(req, res) {
	if (req.body) {
		contactDetails.updateContactDetail(req.body, res);
	} else {
		res.status(500).send({
			'message' : 'Please provide details'
		});
	}
});

app.get('/deleteContactDetail', function(req, res) {
	if (req.query && req.query.contactNumber) {
		contactDetails.deleteContact(req, res);
	} else {
		res.status(500).send({
			'message' : 'Please enter contact number'
		});
	}
})

app.get('/searchContactDetails', function(req, res) {;
	if (req.query && req.query.contactNumber) {
		contactDetails.searchContact(req, res);
	} else {
		res.status(500).send({
			'message' : 'Please enter contact number'
		});
	}
})


//Serve 404 for all other request except above.
app.all('*', function(req, res) {
	var errMsg = {
		'message' : 'Not Found'
	}
	res.status(404).send(errMsg)
})


app.listen(3000, function() {
	console.log("Listening on port 3000");
})